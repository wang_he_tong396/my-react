import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Counter extends Component {

  constructor (props) {
    super(props)

    this.onClickIncrementButton = this.onClickIncrementButton.bind(this)
    this.onClickDecrementButton = this.onClickDecrementButton.bind(this)
    
    this.state = {
      count: props.initValue
    }
  }

  componentWillMount () {
    console.log('enter componentWillMount ' + this.props.caption)
  }

  componentWillReceiveProps (nextProps) {
    console.log('enter componentWillReceiveProps ' + this.props.caption)
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (nextProps.caption !== this.props.caption) || (nextState.count !== this.state.count)
  }

  onClickIncrementButton () {
    this.updateCount(true)
  }

  onClickDecrementButton () {
    this.updateCount(false)
  }

  updateCount (isIncrement) {
    const previousValue = this.state.count
    const newValue = isIncrement ? previousValue + 1 : previousValue - 1
    this.setState({
      count: newValue
    })
    this.props.onUpdate(newValue, previousValue)
  }

  render () {
    const { caption } = this.props
    const buttonStyle = {
      marginRight: '20px',
      marginBottom: '10px'
    }
    return (
      <div>
        <button style={buttonStyle} onClick={this.onClickIncrementButton} >+</button>
        <button style={buttonStyle} onClick={this.onClickDecrementButton} >-</button>
        <span> { caption } count: { this.state.count }</span>
      </div>
    )
  }
}

Counter.propTypes = {
  caption: PropTypes.string.isRequired,
  initValue: PropTypes.number,
  onUpdate: PropTypes.func
}

Counter.defaultProps = {
  initValue: 0,
  onUpdate: f => f
}

class ControlPanel extends Component {
  constructor (props) {
    super(props)
    this.onCounterUpdate = this.onCounterUpdate.bind(this)
    this.initValues = [
      0, 10, 20
    ]
    const initSum = this.initValues.reduce((a, b) => a + b, 0)
    this.state = {
      sum: initSum
    }
  }

  onCounterUpdate (newValue, previousValue) {
    console.log('onUpdate')
    const valueChange = newValue - previousValue
    this.setState({
      sum: this.state.sum + valueChange
    })
  }

  render () {
    const counterStyle = {
      margin: '16px'
    }
    const aStyle = {
      position: 'fixed',
      bottom: '10px'
    }
    return (
      <div style={counterStyle}>
        <Counter onUpdate={this.onCounterUpdate} caption="First" initValue={this.initValues[0]} />
        <Counter onUpdate={this.onCounterUpdate} caption="Second" initValue={this.initValues[1]} />
        <Counter onUpdate={this.onCounterUpdate} caption="Third" initValue={this.initValues[2]} />
        {/* <button onClick={ () => this.forceUpdate() }>
          Click me to repaint!
        </button> */}
        <div>
          Total count: { this.state.sum }
        </div>
        <a style={aStyle} href="http://beian.miit.gov.cn/">鲁ICP备20024443号-1</a>
      </div>
    )
  }
}

export default ControlPanel