import { object } from "prop-types"
import AppDispatcher from '../AppDispatcher.js'

const counterValues = {
  'First': 0,
  'Second': 10,
  'Third': 30
}

const CounterStore = Object.assign({}, EventEmitter.prototype, {
  getCounterVaules: function () {
    return counterValues
  },
  emitChage: function () {
    this.emit(CHANGE_EVENT)
  },
  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback)
  }
})

CounterStore.dispatchToken = AppDispatcher.register((action) => {
  if (action.type === ActionTypes.INCREMENT) {
    counterValues[action.counterCaption]++
    CounterStore.emitChage()
  } else if (action.type === ActionTypes.DECREMENT) {
    counterValues[action.counterCaption]--
    CounterStore.emitChage()
  }
})